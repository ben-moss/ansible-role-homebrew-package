# Ansible Role: Homebrew Package

An ansible role for managing different kinds of homebrew packages (standard and casks) that supports pre-install of dependent homebrew taps.

## Requirements

This role depends on the installation of homebrew, which is handled via an external dependency.

Due to [lack of compatibility between Ansible Galaxy and GitLab for standalone roles](https://github.com/ansible/galaxy/issues/236), a little more configuration is needed to pull in this role, so the entry in your `requirements.yml` might look like this:

```yaml
- name: homebrew_package
  src: git@gitlab.com:ben-moss/ansible-role-homebrew-package.git
  scm: git
  version: v0.0.1
```

## Role Variables

- `homebrew_package_name` - the package name on homebrew
- `homebrew_package_description` - a more representative description of the package - used to improve task output. Defaults to a capitalized version of `homebrew_package_name`
- `homebrew_package_required_taps` - a list of required homebrew taps. Default is no taps.

## Dependencies

- `danbohea.homebrew`

## Usage Examples

### Example Playbooks

The most basic usage only requires the package name:

```yaml
- hosts: workstation
  roles:
     - name: homebrew_package
       homebrew_package_name: shellcheck
```

If the package requires any dependent homebrew taps installing, they can be specified as a list:

```yaml
- hosts: workstation
  roles:
     - name: homebrew_package
       homebrew_package_name: garmin-express
       homebrew_required_taps:
         - homebrew/cask-drivers
```

If the package name is not very recognizable, an alternative can be provided for more helpful output:

```yaml
- hosts: workstation
  roles:
     - name: homebrew_package
       homebrew_package_name: dat
       homebrew_package_description: 'Dat Desktop'
```

### Example Role Dependency

If the package is being installed as a meta dependency of another role:

```yaml
dependencies:
  - role: homebrew_package
    homebrew_package_name: google-chrome
    homebrew_package_description: 'Google Chrome'
```

## License

MIT

## Author Information

Ben Moss
